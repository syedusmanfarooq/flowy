export interface Shape {
    id: string;
    type: string;
    x: number;
    y: number;
    rx: number;
    ry: number;
    width: number;
    height: number;
    fill: string;
    stroke: string;
    stroke_width: number;
    r: number;
    transform: string;
    points:string;
  
}
export class Rectangle implements Shape {
    points="";
    type='rectangle';
    r=0;
    x= 20;
    y= 20;
    rx= 10;
    ry= 10;
    width=100;
    height= 100;
    fill= '#297ab5';
    stroke='black';
    id='';
    stroke_width=3;
    transform='skewX(0) skewY(0)';
   
    constructor(
        _id:string
         ) {
           this.id= _id; 
    }
}
export class Circle implements Shape {
    type='circle';
    points="";
    r=50;
    x= 50;
    y= 50;
    rx= 10;
    ry= 10;
    width=100;
    height= 100;
    fill= '#297ab5';
    stroke='black';
    id='';
    stroke_width=3;
    transform='skewX(0) skewY(0)';
   
    constructor(
        _id:string,_r:number
         ) {
           this.id= _id;
           this.r= _r;  
    }

}
export class Text implements Shape {
    type='text';
    points="";
    r=50;
    x= 50;
    y= 50;
    rx= 10;
    ry= 10;
    width=100;
    height= 100;
    fill= '#fff';
    stroke='black';
    id='';
    innerHtml='';
    font_size=15;
    font_weight='bold';
    stroke_width=3;
    transform='skewX(0) skewY(0)';
    constructor(
        _id:string,text:string
         ) {
           this.id= _id;
           this.innerHtml= text;  
    }

}
export class Line implements Shape {
    type='line';
    r=50;
    rx= 10;
    ry= 10;
    width=100;
    height= 100;
    fill= 'none';
    stroke='white';
    id='';
    innerHtml='';
    font_size=15;
    font_weight='bold';
    stroke_width=2;
    transform='skewX(0) skewY(0)';
    x=0;
    y=0;
    points='';
    
    public set marker_end(v : string) {
        this.marker_end= v;
    }
    
    // public set points(v : string) {
    //     this.points = v;
    // }
    
    
    
    constructor(
        _id:string
         ) {
           this.id= _id;
    }

}