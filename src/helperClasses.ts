import * as vscode from "vscode";

export class HelperCommand implements vscode.Command {
	constructor(public title: string, public command: string,public args:any[]) {
	}
}

export function GroupId(): string {
	return "Group_" + String.fromCharCode(Math.floor((Math.random() * 27) + 97), Math.floor((Math.random() * 27) + 65), Math.floor((Math.random() * 27) + 97)) + Math.floor((Math.random() * 1000) + 1);
}
export function PanelId(): string {
	return "Panel_" + String.fromCharCode(Math.floor((Math.random() * 27) + 97), Math.floor((Math.random() * 27) + 65), Math.floor((Math.random() * 27) + 97)) + Math.floor((Math.random() * 1000) + 1);
}
export function SelectionCases(type: string, data: any): any[] {
	let ribbon: any[];
	ribbon = [];
	switch (type) {
		case 'rectangle': {
			return rectangleRibbon(data);
		}
		case 'circle': {
			return circleRibbon(data);
		}
		case 'text': {
			return TextRibbon(data);
		}
		case 'line': {
			return LineRibbon(data);
		}
	}
	return ribbon;
}
function rectangleRibbon(data: any): any[] {

	const rectangleRibbonOptions = [{ command: '', type: 'width', text: 'width' },
	{ command: 'shapes.increaseWidth', type: 'width', text: '$(chevron-up)' },
	{ command: 'shapes.decreaseWidth', type: 'width', text: '$(chevron-down)' },
	{ command: '', type: 'height', text: 'height' },
	{ command: 'shapes.increaseHeight', type: 'height', text: '$(chevron-up)' },
	{ command: 'shapes.decreaseHeight', type: 'height', text: '$(chevron-down)' },
	{ command: '', type: 'br', text: 'border-radius' },
	{ command: 'shapes.increasebr', type: 'br', text: '$(chevron-up)' },
	{ command: 'shapes.decreasebr', type: 'br', text: '$(chevron-down)' },
	{ command: 'shapes.color', type: 'color', text: 'color' }];
	return rectangleRibbonOptions;

}
function circleRibbon(data: any): any[] {

	const rectangleRibbonOptions = [{ command: '', type: 'r', text: 'radius' },
	{ command: 'shapes.increaseRadius', type: 'r', text: '$(chevron-up)' },
	{ command: 'shapes.decreaseRadius', type: 'r', text: '$(chevron-down)' },
	{ command: 'shapes.color', type: 'color', text: 'color' }];
	return rectangleRibbonOptions;

}
function TextRibbon(data: any): any[] {

	const rectangleRibbonOptions = [{ command: '', type: 'font-size', text: 'font' },
	{ command: 'shapes.increaseFont', type: 'font-size', text: '$(chevron-up)' },
	{ command: 'shapes.decreaseFont', type: 'font-size', text: '$(chevron-down)' },
	{ command: 'shapes.color', type: 'color', text: 'color' }];
	return rectangleRibbonOptions;

}
function LineRibbon(data: any): any[] {

	const rectangleRibbonOptions = [
	{ command: 'shapes.color', type: 'color', text: 'color' }];
	return rectangleRibbonOptions;

}
export class ShapeOption extends vscode.TreeItem {
	iconPath={light:'', dark:''};

	constructor(public readonly label: string, public readonly collapsibleState: vscode.TreeItemCollapsibleState, public readonly command?: vscode.Command) {
		super(label, collapsibleState);
	}
}
// class RibbonItem implements vscode.StatusBarItem{
// 	alignment: vscode.StatusBarAlignment;	priority: number;
// 	text: string;
// 	tooltip: string | undefined;
// 	color: string | vscode.ThemeColor | undefined;
// 	command: string | undefined;
// 	constructor(alignment:vscode.StatusBarAlignment,priority:number, text:string){
// 		this.alignment=alignment;
// 		this.priority=priority;
// 		this.text=text;
// 	}
// 	show(): void {
// 		// return this;
// 		// this.show();
// 	}
// 	hide(): void {

// 	}
// 	dispose(): void {

// 	}


// }