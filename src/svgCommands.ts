import * as vscode from "vscode";
export class SvgFunctions {
    public createSvg(): vscode.WebviewPanel {
        const panel = vscode.window.createWebviewPanel('Draw', 'Draw', vscode.ViewColumn.One, {
            enableScripts: true
        });
        panel.title = "Drawing";
        // Display a message box to the user
        panel.webview.html = this.getWebviewContent();
        return panel;
    }
    private messageReciver(): string {
        return `
        window.addEventListener('message', event => {
            let data = event.data;
            if(data.command==='create'){
                createSVGandShape(data);
            }else if(data.command==='increase'){
                selection.setAttribute(data.type,data.value);
                let bbox=selection.getBBox();
                if(selection.nodeName==='circle'){
                   if(bbox.x<0){
                    selection.setAttribute('cx',bbox.width );
                   }else if(bbox.y<0){
                    selection.setAttribute('cy',bbox.height );
                   } 
                }
            }else if(data.command==='decrease'){
                selection.setAttribute(data.type,data.value);
            }else if(data.command==='color'){
                selection.setAttribute(data.type,data.value);
            }else if(data.command==='save_jpeg'){
                saveToJpeg()
            }
            
        });
        `;
    }
    private createSvgDataString(): string {
        return `function saveToJpeg() {
            let svgTag = document.querySelector('svg');

            if (document.querySelector('canvas')) {
                document.body.removeChild('canvas')
            }
            let canvas = document.createElement('canvas');
            canvas.setAttribute('width',  svgTag.attributes.width.value) ;
            canvas.setAttribute('height',  svgTag.attributes.height.value) ;
            let svg_xml = (new XMLSerializer()).serializeToString(svgTag);
            var ctx = canvas.getContext('2d');
            var img = new Image();
            img.src = "data:image/svg+xml;base64," + btoa(svg_xml);

            img.onload = function () {
                ctx.drawImage(img, 0, 0);
                var dt = canvas.toDataURL('image/jpeg');
                vscode.postMessage({
                    command: 'save_jpeg', dataString:dt
                });            
            }
        }`;
    }
    private createTextFunctionAndEvent(): string {
        return `
        function createText(data){
        let group = createSvgElement('g');
      
        let text_element = createSvgElement('text');
        text_element.setAttribute('id', data.shape.id);
        text_element.setAttribute('x', data.shape.x)
        text_element.setAttribute('y', data.shape.y)
        text_element.setAttribute('fill', data.shape.fill)
        text_element.setAttribute('font-size', data.shape.font_size)
        text_element.setAttribute('font-weight', data.shape.font_weight)
        text_element.setAttribute('stroke', 'none')
        text_element.innerHTML = data.shape.innerHtml;
        group.appendChild(text_element);
        group.onmousedown = (e) => {
            selection = group.querySelector('text');
           
            let obj ={id:selection.getAttribute('id'),font:selection.getAttribute('font-size')};
            if (!selection.classList.contains('selected') ) {
                if( document.querySelector('.selected')){
                    document.querySelector('.selected').classList.remove('selected');
                }
                vscode.postMessage({
                    command: 'selection', data:obj, type:'text', panel_id :PanelId
                });
                selection.classList.add('selected')
            } else {
                selection.classList.remove('selected')
                selection = null;
                vscode.postMessage({
                    command: 'unselection', panel_id :PanelId
                });
            }
        }
        svg.appendChild(group)
    }
        `;
    }
    private createCircleFunctionAndEvent(): string {
        return `
        function createCircle(data){
        let group = createSvgElement('g');
       
        let circle = createSvgElement('circle');
        circle.setAttribute('id', data.shape.id);
        circle.setAttribute('cx',data.shape.x);
        circle.setAttribute('cy', data.shape.y);
        circle.setAttribute('r', data.shape.r);
        circle.setAttribute('fill', data.shape.fill);
        circle.setAttribute('stroke', data.shape.stroke);
        circle.setAttribute('stroke-width', data.shape.stroke_width);
        circle.setAttribute('transform', data.shape.transform);
                group.onclick = () => {

        }
        group.appendChild(circle);
        group.onmousedown = (e) => {
            selection = group.querySelector('circle');
           
            let obj ={id:selection.getAttribute('id'),r:selection.getAttribute('r')};
            if (!selection.classList.contains('selected') ) {
                if( document.querySelector('.selected')){
                    document.querySelector('.selected').classList.remove('selected');
                }
                vscode.postMessage({
                    command: 'selection', data:obj, type:'circle', panel_id :PanelId
                });
                selection.classList.add('selected')
                
            } else {
                selection.classList.remove('selected')
                
                selection = null;
                vscode.postMessage({
                    command: 'unselection', panel_id :PanelId
                });
            }
        }
        group.onmouseup = (e) => {
        }
        group.onmousemove = (e) => {
        
        }
           svg.insertBefore(group, svg.childNodes[0] || null);
           document.body.appendChild(svg);
    }
           `;
    }
    private createSvgTag(): string {
        return `
        function createMainSVG(data){
            svg =  createSvgElement(data.type);
        svg.setAttribute('width', "1000");
        svg.setAttribute('height', "600");
       
        ${this.createdefs()}
        document.body.appendChild(svg);
        }
        `;
    }
    private createSvgMouseEvent(): string {
        return `
        svg.onmousedown=(e)=>{
            e.preventDefault();
            if(line){
                svg.removeChild(document.querySelector('#mt'));
                svg.removeChild(document.querySelector('#mr'));
                var pt = svg.createSVGPoint();
            pt.x = e.clientX;
            pt.y = e.clientY;
            var cursorpt = pt.matrixTransform(svg.getScreenCTM().inverse());
           

            path = createSvgElement('polyline');
            path.setAttribute('id', lineData.id)
            path.setAttribute('points', cursorpt.x + ',' + cursorpt.y);
            path.setAttribute('stroke', lineData.stroke);
            path.setAttribute('stroke-width', lineData.stroke_width);
            path.setAttribute('fill', lineData.fill);
            path.setAttribute('selected', 'false');
            path.setAttribute('marker-start', "url(#dot)")
            path.onmousedown = (e) => {
                
                selection = e.target;
                if(selection.getAttribute('selected')=='false'){
                    let obj ={id:selection.getAttribute('id')};
                    path.setAttribute('selected', 'true');
                    vscode.postMessage({
                        command: 'selection', data:obj, type:'line', panel_id :PanelId
                    });
                }else{
                    path.setAttribute('selected', 'false');
                    selection=null;
                    vscode.postMessage({
                        command: 'unselection', panel_id :PanelId
                    });
                }
             
            }
            svg.appendChild(path);
            svg.onmousedown = null;
            }
        }
        `;
    }
    private createdefs(): string {
        return `
        let defs = createSvgElement('defs')
        let circle_mark = createSvgElement('circle');
        let upPoly = createSvgElement('polygon');
        let downPoly = createSvgElement('polygon');
        let leftPoly = createSvgElement('polygon');
        let rightPoly = createSvgElement('polygon');
        upPoly.setAttribute('points', "5 0, 10 10, 0 10");
        downPoly.setAttribute('points', "0 0, 10 0, 5 5");
        leftPoly.setAttribute('points', "0 0, 0 10, 5 5");
        rightPoly.setAttribute('points', "10 0, 10 10, 0 5");
        upPoly.setAttribute('fill', 'red')
        downPoly.setAttribute('fill', 'red')
        leftPoly.setAttribute('fill', 'red')
        rightPoly.setAttribute('fill', 'red')
        circle_mark.setAttribute('cx', '5')
        circle_mark.setAttribute('cy', '5')
        circle_mark.setAttribute('r', '2')
        circle_mark.setAttribute('fill', '#3eb74c')
        let marker_circle = createSvgElement('marker')
        marker_circle.setAttribute('id', "dot")
        marker_circle.setAttribute('viewBox', "0 0 10 10")
        marker_circle.setAttribute('refX', "5")
        marker_circle.setAttribute('refY', "5")
      
        marker_circle.setAttribute('markerWidth', "6")
        marker_circle.setAttribute('markerHeight', "6")
        marker_circle.appendChild(circle_mark);
        
        let marker_up = createSvgElement('marker')
        marker_up.setAttribute('id', "up")
        marker_up.setAttribute('viewBox', "0 0 10 10")
        marker_up.setAttribute('refX', "5")
        marker_up.setAttribute('refY', "5")
        marker_up.setAttribute('markerWidth', "6")
        marker_up.setAttribute('markerHeight', "6")
       
        marker_up.appendChild(upPoly);
        

        let marker_down = createSvgElement('marker')
        marker_down.setAttribute('id', "down")
        marker_down.setAttribute('viewBox', "0 0 10 10")
        marker_down.setAttribute('refX', "5")
        marker_down.setAttribute('refY', "5")
        marker_down.setAttribute('markerWidth', "6")
        marker_down.setAttribute('markerHeight', "6")
     
        marker_down.appendChild(downPoly);


        let marker_left = createSvgElement('marker')
        marker_left.setAttribute('id', "left")
        marker_left.setAttribute('viewBox', "0 0 10 10")
        marker_left.setAttribute('refX', "5")
        marker_left.setAttribute('refY', "5")
        marker_left.setAttribute('markerWidth', "6")
        marker_left.setAttribute('markerHeight', "6")
        
        marker_left.appendChild(leftPoly);

        let marker_right = createSvgElement('marker')
        marker_right.setAttribute('id', "right")
        marker_right.setAttribute('viewBox', "0 0 10 10")
        marker_right.setAttribute('refX', "5")
        marker_right.setAttribute('refY', "5")
        marker_right.setAttribute('markerWidth', "6")
        marker_right.setAttribute('markerHeight', "6")
       
        
        marker_right.appendChild(rightPoly);
        
        
        defs.appendChild(marker_circle);
        defs.appendChild(marker_up);
        defs.appendChild(marker_down);
        defs.appendChild(marker_left);
        defs.appendChild(marker_right);
        svg.appendChild(defs)
        `;
    }
    private createHelperFunctionsAndDeclaration(): string {
        return `
        const vscode = acquireVsCodeApi();
        let svg = {};
        let path = null;
        let line = false;
        let selection = null;
      
        let LastKeyDown= null;
        let lineData= null;
        let PanelId=null;
        function createSvgElement(type) {
            return document.createElementNS('http://www.w3.org/2000/svg', type)
        }
        function getSVGMeasure(_svg) {
            return {
                height: parseInt(_svg.getAttribute('height')),
                width: parseInt(_svg.getAttribute('width'))
            }
        }
        function createSVGandShape(data){
            switch (data.type) {
                case 'svg':{
                    PanelId=data.id
                    createMainSVG(data);
                }  
                break;
                case 'rectangle':{
                    createRectangle(data);
                }
                break;
                case 'circle':{
                    createCircle(data);
                }
                break;
                case 'text':{
                    createText(data);
                }
                break;
                case 'line':{
                    lineData= data.shape;
                    selection.classList.remove('selected')
                    selection = null;
                    vscode.postMessage({
                        command: 'unselection', panel_id :PanelId
                    });
                    createMessageRect();
                }
                break;
            }
        }
        ${this.createSvgTag()}
        ${this.lineMessageFunction()}
        ${this.lineKeyDownComputation()}
        ${this.createRectangleFunctionAndEvent()}
        ${this.createCircleFunctionAndEvent()}
        ${this.createTextFunctionAndEvent()}
        ${this.createSvgDataString()}
        `;
    }
    private lineMessageFunction(): string {
        return `
        function createMessageRect() {
            let measure = getSVGMeasure(svg);
            let messageRect = createSvgElement('rect')
            messageRect.setAttribute('id', 'mr')
            messageRect.setAttribute('x', '0')
            messageRect.setAttribute('y', '0')
            messageRect.setAttribute('width', measure.width)
            messageRect.setAttribute('height', measure.height)
            messageRect.setAttribute('fill', '#297ab5')
            messageRect.setAttribute('fill-opacity', '0.3');
            let text = createSvgElement('text');
            text.setAttribute('x', measure.width / 4)
            text.setAttribute('y', measure.height / 2)
            text.setAttribute('class', 'heavy');
            text.setAttribute('id', 'mt');
            text.setAttribute('fill', '#fff');
            text.innerHTML = "Click any where on the highlighted area to start drawing a line."+"<tspan x='"+measure.width / 2.5+"' y='"+measure.height / 1.8+"'>"+"Press ESC to exit line draw.</tspan>"
            svg.appendChild(messageRect);
            svg.appendChild(text);
            svg.focus();
            ${this.createSvgMouseEvent()}
            line=true;
        }`;
    }

    private lineKeyDownComputation(): string {
        return `
        function lineDraw(e, path, currentSVG) {
            let pointsString = path.getAttribute('points')
            let points = pointsString.split(" ")
            let lastPoint = points[points.length - 1];
            let lastPointArray = lastPoint.split(',');
            if (e.key == "ArrowDown") {
                LastKeyDown=e.key;
                let cp_point = parseInt(lastPointArray[1]) + 20;
                path.setAttribute('points', pointsString +" "+ lastPointArray[0] + ',' +cp_point  )
            } else if (e.key == "ArrowUp") {
                LastKeyDown=e.key;
                let cp_point =parseInt(lastPointArray[1]) - 20;
                path.setAttribute('points', pointsString +" "+ lastPointArray[0] + ',' + cp_point)
            } else if (e.key == "ArrowRight") {
                LastKeyDown=e.key;
                let cp_point =parseInt(lastPointArray[0]) + 20;
                path.setAttribute('points', pointsString +" "+ cp_point  + ',' + parseInt(lastPointArray[1]))
            } else if (e.key == "ArrowLeft") {
                LastKeyDown=e.key;
                let cp_point =parseInt(lastPointArray[0]) - 20;
                path.setAttribute('points', pointsString +" "+cp_point+','+parseInt(lastPointArray[1]))
            } else if (e.key == "Escape") {
               
                if (line && points.length==1) {
                   
                    svg.removeChild(document.querySelector("#"+path.getAttribute(lineData.id)));
                    vscode.postMessage({
                        command: 'line_delete', lineId:lineData.id
                    });
                  
                }else{
                    
                    if(LastKeyDown=="ArrowUp"){
                        path.setAttribute('marker-end', "url(#up)")
                     
                    }else if(LastKeyDown=="ArrowDown"){
                        path.setAttribute('marker-end', "url(#down)")
                      
                    }else if(LastKeyDown== "ArrowLeft"){
                        path.setAttribute('marker-end', "url(#right)")
                      
                    }else if(LastKeyDown == "ArrowRight"){
                        path.setAttribute('marker-end', "url(#left)")
                    
                    }
                }
                lineData.points=pointsString;
                vscode.postMessage({
                    command: 'line_data', line:lineData, panel_id:PanelId
                });
                line=false;
                path=null; 
                LastKeyDown=null;
                lineData=null;
            }
            if(path){currentSVG.appendChild(path)}
        }`;
    }
    private createRectangleFunctionAndEvent(): string {
        return `
        function createRectangle(data){
        let group = createSvgElement('g');
        let rectangle = createSvgElement('rect');
        rectangle.setAttribute('id', data.shape.id);
        rectangle.setAttribute('x', data.shape.x);
        rectangle.setAttribute('y', data.shape.y);
        rectangle.setAttribute('rx', data.shape.rx);
        rectangle.setAttribute('ry', data.shape.ry);
        rectangle.setAttribute('width', data.shape.width);
        rectangle.setAttribute('height', data.shape.height);
        rectangle.setAttribute('fill', data.shape.fill);
        rectangle.setAttribute('stroke', data.shape.stroke);
        rectangle.setAttribute('stroke-width', data.shape.stroke_width);
        rectangle.setAttribute('transform', data.shape.transform);
        group.onclick = () => {

        }
        group.appendChild(rectangle);
        group.onmousedown = (e) => {
            selection = group.querySelector('rect');
            
            let obj ={id:selection.getAttribute('id'),width:selection.getAttribute('width'),height:selection.getAttribute('height')};
            if (!selection.classList.contains('selected') ) {
                if( document.querySelector('.selected')){
                    document.querySelector('.selected').classList.remove('selected');
                }
                vscode.postMessage({
                    command: 'selection', data:obj, type:'rectangle', panel_id :PanelId
                });
                selection.classList.add('selected')
            } else {
                selection.classList.remove('selected')
                selection = null;
                vscode.postMessage({
                    command: 'unselection', panel_id :PanelId
                });
            }
        }
        group.onmouseup = (e) => {
        }
        group.onmousemove = (e) => {
        
        }
           svg.insertBefore(group, svg.childNodes[0] || null);
           document.body.appendChild(svg);
        }
       `;
    }
    private createKeyDownEvent(): string {
        return `
        window.onkeydown = (e) => {
            e.preventDefault()
            let currentSVG = svg;
            let SVGMeasure = getSVGMeasure(currentSVG);
            if (selection) {
                if (e.key == "ArrowDown") {
                    if (selection.tagName == 'rect' || selection.tagName == 'text') {
                        let newYCoord = parseInt(selection.getAttribute('y')) + 5;
                        if (newYCoord > SVGMeasure.height) {
                            currentSVG.setAttribute('height', SVGMeasure.height + 200);
                        }
                        selection.setAttribute('y', newYCoord);
                        vscode.postMessage({
                            command: 'changeY', y: newYCoord
                        });
                    } else if (selection.tagName == 'circle') {
                        let newYCoord = parseInt(selection.getAttribute('cy')) + 5;
                        if (newYCoord > SVGMeasure.height) {
                            currentSVG.setAttribute('height', SVGMeasure.height + 200);
                        }
                        selection.setAttribute('cy', newYCoord);
                        vscode.postMessage({
                            command: 'changeY', y: newYCoord
                        });
                    }

                } else if (e.key == "ArrowUp") {
                    if (selection.tagName == 'rect' || selection.tagName == 'text') {
                        let newYCoord = parseInt(selection.getAttribute('y')) - 5;
                        if (newYCoord <= 10) {
                            newYCoord=10;
                            selection.setAttribute('y', 10);
                        } else {
                            selection.setAttribute('y', newYCoord);
                        }
                        vscode.postMessage({
                            command: 'changeY', y: newYCoord
                        });
                    } else if (selection.tagName == 'circle') {
                        let newYCoord = parseInt(selection.getAttribute('cy')) - 5;
                        if (newYCoord <= 10) {
                            newYCoord=10
                            selection.setAttribute('cy', 10);
                        } else {
                            selection.setAttribute('cy', newYCoord);
                        }
                        vscode.postMessage({
                            command: 'changeY', y: newYCoord
                        });
                    }
                } else if (e.key == "ArrowRight") {
                    if (selection.tagName == 'rect' || selection.tagName == 'text') {
                        let newXCoord = parseInt(selection.getAttribute('x')) + 5;
                        if (newXCoord > SVGMeasure.width) {
                            currentSVG.setAttribute('width', SVGMeasure.width + 200);
                        }
                        selection.setAttribute('x', newXCoord);
                        vscode.postMessage({
                            command: 'changeX', x: newXCoord
                        });
                    } else if (selection.tagName == 'circle') {
                        let newXCoord = parseInt(selection.getAttribute('cx')) + 5;
                        if (newXCoord > SVGMeasure.width) {
                            currentSVG.setAttribute('width', SVGMeasure.width + 200);
                        }
                        selection.setAttribute('cx', newXCoord);
                        vscode.postMessage({
                            command: 'changeX', x: newXCoord
                        });
                    }
                } else if (e.key == "ArrowLeft") {
                    if (selection.tagName == 'rect' || selection.tagName == 'text') {
                        let newXCoord = parseInt(selection.getAttribute('x')) - 5;
                        if (newXCoord <= 10) {
                            newXCoord=10;
                            selection.setAttribute('x', 10);
                        } else {
                            selection.setAttribute('x', newXCoord);
                        }
                        vscode.postMessage({
                            command: 'changeX', x: newXCoord
                        });
                    } else if (selection.tagName == 'circle') {
                        let newXCoord = parseInt(selection.getAttribute('cx')) - 5;
                        if (newXCoord <= 10) {
                            newXCoord=10
                            selection.setAttribute('cx', 10);
                        } else {
                            selection.setAttribute('cx', newXCoord);
                        }
                        vscode.postMessage({
                            command: 'changeX', x: newXCoord
                        });
                    }
                }else if (e.key == "Escape"){
                    if(selection){
                        selection.setAttribute('stroke', 'black')
                        selection=null;
                        //making selection null
                    }
                    
                }
            } else {
                if (line && path) {
                    lineDraw(e, path, currentSVG);
                }
            }
        
        }
        `;
    }
    private getWebviewContent() {
        return `
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Drawing</title>
            <style>
                circle,text,rect, polyline{
                   cursor: pointer;
                 }
                .heavy { font: bold 20px sans-serif;  }
                .selected{
                    stroke: #b30000;
                }
            </style>
        </head>
        <body>
        </body>
        <script>
        ${this.createHelperFunctionsAndDeclaration()}
        ${this.messageReciver()}
        ${this.createKeyDownEvent()}
        </script>
        </html>
        `;
    }

}