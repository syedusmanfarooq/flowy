import * as vscode from "vscode";
let path= require('path');
import { ShapeOption,HelperCommand } from './helperClasses';

export class ShapeProvider implements vscode.TreeDataProvider<ShapeOption> {


    refresh(): void {
    }

    getTreeItem(element: ShapeOption): vscode.TreeItem {
        return element;
    }

    getChildren(element?: ShapeOption): Thenable<ShapeOption[]> {
        return new Promise(resolve => {
            let circle =new ShapeOption('circle', vscode.TreeItemCollapsibleState.None, new HelperCommand('createCircle', 'shapes.createCircle',[]));
            circle.iconPath.light=path.join(__filename, '..', '..', 'media', 'circle.svg');
            circle.iconPath.dark=path.join(__filename, '..', '..', 'media', 'circle.svg');
            let ShapesOptions = [circle,
             new ShapeOption('square', vscode.TreeItemCollapsibleState.None,new HelperCommand('createRectangle', 'shapes.createRectangle',[])),
             new ShapeOption('text', vscode.TreeItemCollapsibleState.None,new HelperCommand('createText', 'shapes.createText',[])),
             new ShapeOption('line', vscode.TreeItemCollapsibleState.None,new HelperCommand('createLine', 'shapes.createLine',[]))];
             resolve(ShapesOptions);
        });
    }
}
export class SelectionProvider implements vscode.TreeDataProvider<ShapeOption> {
    _Type:string;
    constructor(type:string){
        this._Type=type;
    }
    refresh(): void {
    }

    getTreeItem(element: ShapeOption): vscode.TreeItem {
        return element;
    }

    getChildren(element?: ShapeOption): Thenable<ShapeOption[]> {
        let ShapesCommandOptions:ShapeOption[]=[];
        if(this._Type==='rectangle'){
            ShapesCommandOptions= [new ShapeOption('width', vscode.TreeItemCollapsibleState.None, new HelperCommand('width', 'selection.width',['width'])),
            new ShapeOption('height', vscode.TreeItemCollapsibleState.None,new HelperCommand('height', 'selection.height',['height'])),
            new ShapeOption('border radius', vscode.TreeItemCollapsibleState.None,new HelperCommand('border radius', 'selection.border_radius',['rx'])),
           ];
        }else if(this._Type==='circle'){
            ShapesCommandOptions= [new ShapeOption('radius', vscode.TreeItemCollapsibleState.None, new HelperCommand('radius', 'selection.radius',['r'])),
            ];
        }else if(this._Type==='text'){
            ShapesCommandOptions= [new ShapeOption('font', vscode.TreeItemCollapsibleState.None, new HelperCommand('font', 'selection.font',['font-size'])),
        ];
        }else if(this._Type==='line'){

        }
        return new Promise(resolve => {
             resolve(ShapesCommandOptions);
        });
    }
}