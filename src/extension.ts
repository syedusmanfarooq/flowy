'use strict';

import * as vscode from 'vscode';
let fs = require("fs");
import { ShapeProvider, SelectionProvider } from './mainShapesProvider';
import { SvgFunctions } from './svgCommands';
import { GroupId, SelectionCases } from './helperClasses';
let panels: vscode.WebviewPanel[];
panels = [];
let ribbon: vscode.StatusBarItem[];
ribbon = [];
import { Shape, Rectangle, Circle, Text, Line } from './shapes';
let Activepanel: number = 0;
let AllShapes: Shape[][];
AllShapes = [];
let CurrentSelectionTree: vscode.Disposable;
let selection: any = {};

export function activate(context: vscode.ExtensionContext) {

  
    let mainShapesProvider = new ShapeProvider();
    let mainSelectionProvider: SelectionProvider[] = [new SelectionProvider('rectangle'), new SelectionProvider('circle'), new SelectionProvider('text'), new SelectionProvider('line')];
    let svg = new SvgFunctions();
    vscode.window.registerTreeDataProvider('shapes', mainShapesProvider);

    vscode.commands.registerCommand('shapes.createNewWindow', () => {
        let panel_object = svg.createSvg();
        panel_object.webview.onDidReceiveMessage(message => {
            switch (message.command) {
                case 'line_data': {
                    let selected_line = AllShapes[message.panel_id].filter(shape => {
                        if (shape.id === message.line.id) {
                            return shape;
                        }
                    });
                    selected_line[0].points = message.line.points;
                }
                    break;
                case 'line_delete': {
                    AllShapes[message.panel_id] = AllShapes[message.panel_id].filter(shape => {
                        if (shape.id !== message.line.id) {
                            return shape;
                        }
                    });
                }
                    break;
                case 'selection': {
                    selection = getSelection(message);
                    if (selection.type === 'rectangle') {
                        if (CurrentSelectionTree) {
                            CurrentSelectionTree.dispose();
                        }
                        CurrentSelectionTree = vscode.window.registerTreeDataProvider('selection', mainSelectionProvider[0]);
                    } else if (selection.type === 'circle') {
                        if (CurrentSelectionTree) {
                            CurrentSelectionTree.dispose();
                        }
                        CurrentSelectionTree = vscode.window.registerTreeDataProvider('selection', mainSelectionProvider[1]);
                    } else if (selection.type === 'text') {
                        if (CurrentSelectionTree) {
                            CurrentSelectionTree.dispose();
                        }
                        CurrentSelectionTree = vscode.window.registerTreeDataProvider('selection', mainSelectionProvider[2]);
                    } else if (selection.type === 'line') {
                        if (CurrentSelectionTree) {
                            CurrentSelectionTree.dispose();
                        }
                        CurrentSelectionTree = vscode.window.registerTreeDataProvider('selection', mainSelectionProvider[3]);
                    }
                    Activepanel = message.panel_id;
                    if (ribbon.length === 0) {
                        let values = SelectionCases(message.type, message.data);
                        values.forEach(ribbon_item => {

                            const status = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 10);
                            if (!ribbon_item.command && ribbon_item.type === "width") {
                                status.text = ribbon_item.text + " " + selection.width;
                            } else if (!ribbon_item.command && ribbon_item.type === "height") {
                                status.text = ribbon_item.text + " " + selection.height;
                            } else if (!ribbon_item.command && ribbon_item.type === "br") {
                                status.text = ribbon_item.text + " " + selection.rx;
                            } else if (!ribbon_item.command && ribbon_item.type === "r") {
                                status.text = ribbon_item.text + " " + selection.r;
                            } else if (!ribbon_item.command && ribbon_item.type === "font-size") {
                                status.text = ribbon_item.text + " " + selection.font_size;
                            }
                            else {
                                status.text = ribbon_item.text;
                            }
                            status.command = ribbon_item.command;

                            status.show();
                            context.subscriptions.push(status);
                            ribbon.push(status);
                        });
                    } else {
                        ribbon.forEach(ribbon_item => {
                            ribbon_item.dispose();
                        });
                        ribbon = [];
                        let values = SelectionCases(message.type, message.data);
                        values.forEach(ribbon_item => {

                            const status = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 10);
                            status.command = ribbon_item.command;
                            if (!ribbon_item.command && ribbon_item.type === "width") {
                                status.text = ribbon_item.text + " " + selection.width;
                            } else if (!ribbon_item.command && ribbon_item.type === "height") {
                                status.text = ribbon_item.text + " " + selection.height;
                            } else if (!ribbon_item.command && ribbon_item.type === "br") {
                                status.text = ribbon_item.text + " " + selection.rx;
                            } else if (!ribbon_item.command && ribbon_item.type === "r") {
                                status.text = ribbon_item.text + " " + selection.r;
                            } else if (!ribbon_item.command && ribbon_item.type === "font-size") {
                                status.text = ribbon_item.text + " " + selection.font_size;
                            }
                            else {
                                status.text = ribbon_item.text;
                            }
                            status.show();
                            context.subscriptions.push(status);
                            ribbon.push(status);
                        });
                    }

                }
                    break;
                case 'changeY': {
                    selection.y = message.y;
                }
                    break;
                case 'changeX': {
                    selection.x = message.x;
                }
                    break;
                case 'save_jpeg': {
                    var base64Data = message.dataString.replace(/^data:image\/jpeg;base64,/, "");
                    vscode.window.showInputBox({prompt:"path to save file"}).then(val=>{
                        fs.writeFile(val + "/drawing.jpeg", base64Data, 'base64', function (err: Error) {
                            if(err){
                                vscode.window.showErrorMessage('correct path not given.');
                            }
                        });
                    });
                   
                }
                    break;
                case 'unselection': {
                    ribbon.forEach(ribbon_item => {
                        ribbon_item.dispose();
                    });
                    if (CurrentSelectionTree) {
                        CurrentSelectionTree.dispose();
                    }
                    CurrentSelectionTree = vscode.window.registerTreeDataProvider('selection', mainSelectionProvider[3]);
                    ribbon = [];
                    selection = {};
                }
                    break;

            }
        }, undefined, context.subscriptions);
        panels.push(panel_object);
        panels[panels.length - 1].webview.postMessage({ command: "create", type: 'svg', id: panels.length - 1 });
    });
    
    vscode.commands.registerCommand('selection.radius', async () => {
        let value: any = await vscode.window.showInputBox();
        value = Number(value);
        if (value && value > 10) {
            selection.r = value;
            panels[Activepanel].webview.postMessage({ command: "increase", type: 'r', value: selection.r });
        }
    });
    vscode.commands.registerCommand('selection.font', async () => {
        let value: any = await vscode.window.showInputBox();
        value = Number(value);
        if (value && value > 10) {
            selection.font_size = value;
            panels[Activepanel].webview.postMessage({ command: "increase", type: 'font-size', value: selection.font_size });
        }
    });
    vscode.commands.registerCommand('selection.width', async () => {
        let value: any = await vscode.window.showInputBox();
        value = Number(value);
        if (value && value > 10) {
            selection.width = value;
            panels[Activepanel].webview.postMessage({ command: "increase", type: 'width', value: selection.width });
        }
    });
    vscode.commands.registerCommand('selection.height', async () => {
        let value: any = await vscode.window.showInputBox();
        value = Number(value);
        if (value && value > 10) {
            selection.height = value;
            panels[Activepanel].webview.postMessage({ command: "increase", type: 'height', value: selection.height });
        }
    });
    vscode.commands.registerCommand('selection.border_radius', async () => {
        let value: any = await vscode.window.showInputBox();
        value = Number(value);
        if (value && value > 0) {
            selection.rx = value;
            selection.ry = value;
            panels[Activepanel].webview.postMessage({ command: "increase", type: 'rx', value: selection.rx });
            panels[Activepanel].webview.postMessage({ command: "increase", type: 'ry', value: selection.ry });
        }
    });
    vscode.commands.registerCommand('shapes.createText', () => {
        panels.forEach(async (temp_panel, i) => {
            if (temp_panel.active) {
                try {
                    let value = await vscode.window.showInputBox();
                    if (value) {
                        let gid = GroupId();
                        let text = new Text(gid, value);
                        if (AllShapes[i]) {
                            AllShapes[i].push(text);
                        } else {
                            AllShapes[i] = [];
                            AllShapes[i].push(text);
                        }
                        temp_panel.webview.postMessage({ command: "create", type: 'text', shape: text });
                    }

                } catch (e) {
                    console.log(e);
                }
            }
        });

    });
    vscode.commands.registerCommand('shapes.createCircle', () => {
        panels.forEach((temp_panel, i) => {
            if (temp_panel.active) {
                try {
                    let gid = GroupId();
                    let circle = new Circle(gid, 50);
                    if (AllShapes[i]) {
                        AllShapes[i].push(circle);
                    } else {
                        AllShapes[i] = [];
                        AllShapes[i].push(circle);
                    }
                    temp_panel.webview.postMessage({ command: "create", type: 'circle', shape: circle });
                } catch (e) {
                    console.log(e);
                }
            }
        });

    });
    vscode.commands.registerCommand('shapes.createRectangle', () => {
        
        panels.forEach((temp_panel, i) => {
            if (temp_panel.active) {
                try {
                    let gid = GroupId();
                    let rectangle = new Rectangle(gid);
                    if (AllShapes[i]) {
                        AllShapes[i].push(rectangle);
                    } else {
                        AllShapes[i] = [];
                        AllShapes[i].push(rectangle);
                    }
                    temp_panel.webview.postMessage({ command: "create", type: 'rectangle', shape: rectangle });
                } catch (e) {
                    console.log(e);
                }
            }
        });



    });
    vscode.commands.registerCommand('shapes.createLine', () => {
        panels.forEach((temp_panel, i) => {
            if (temp_panel.active) {
                try {
                    let gid = GroupId();
                    let line = new Line(gid);
                    if (AllShapes[i]) {
                        AllShapes[i].push(line);
                    } else {
                        AllShapes[i] = [];
                        AllShapes[i].push(line);
                    }
                    temp_panel.webview.postMessage({ command: "create", type: 'line', index: i, shape: line });
                } catch (e) {
                    console.log(e);
                }
            }
        });
    });



    vscode.commands.registerCommand('shapes.color', async node => {
      await vscode.window.showQuickPick(['blue:#297ab5', 'white:#fcfcfc', 'green:#3eb74c', 'purple:#772eb2', 'lime:#ccbd37'], {
            placeHolder: 'colors',
            onDidSelectItem: item => {

                if (selection.type === 'rectangle' || selection.type === 'circle' || selection.type === 'text') {
                    selection.fill = item.toString().split(':')[1];
                    panels[Activepanel].webview.postMessage({ command: "color", type: 'fill', value: selection.fill });
                } else {
                    selection.stroke = item.toString().split(':')[1];
                    panels[Activepanel].webview.postMessage({ command: "color", type: 'stroke', value: selection.stroke });
                }

            }
        });

    });

    vscode.commands.registerCommand('shapes.increaseFont', node => {
        selection.font_size = selection.font_size + 5;
        ribbon.forEach((ribbon_item, i) => {
            ribbon_item.hide();
            if (i === 0) {
                ribbon_item.text = 'font ' + selection.font_size;
            }
            ribbon_item.show();

        });
        panels[Activepanel].webview.postMessage({ command: "increase", type: 'font-size', value: selection.font_size });
    });
    vscode.commands.registerCommand('shapes.decreaseFont', node => {

        if (selection.font_size - 5 >= 15) {
            selection.font_size = selection.font_size - 5;
            ribbon.forEach((ribbon_item, i) => {
                ribbon_item.hide();
                if (i === 0) {
                    ribbon_item.text = 'font ' + selection.font_size;
                }
                ribbon_item.show();

            });
            panels[Activepanel].webview.postMessage({ command: "decrease", type: 'font-size', value: selection.font_size });
        }

    });

    vscode.commands.registerCommand('shapes.increaseRadius', node => {
        selection.r = selection.r + 5;
        ribbon.forEach((ribbon_item, i) => {
            ribbon_item.hide();
            if (i === 0) {
                ribbon_item.text = 'radius ' + selection.r;
            }
            ribbon_item.show();

        });
        panels[Activepanel].webview.postMessage({ command: "increase", type: 'r', value: selection.r });
    });
    vscode.commands.registerCommand('shapes.decreaseRadius', node => {

        if (selection.r - 5 >= 10) {
            selection.r = selection.r - 5;
            ribbon.forEach((ribbon_item, i) => {
                ribbon_item.hide();
                if (i === 0) {
                    ribbon_item.text = 'radius ' + selection.r;
                }
                ribbon_item.show();

            });
            panels[Activepanel].webview.postMessage({ command: "decrease", type: 'r', value: selection.r });
        }

    });

    vscode.commands.registerCommand('shapes.increasebr', node => {
        selection.rx = selection.rx + 2;
        selection.ry = selection.ry + 2;
        ribbon.forEach((ribbon_item, i) => {
            ribbon_item.hide();
            if (i === 6) {
                ribbon_item.text = 'border-radius ' + selection.rx;
            }
            ribbon_item.show();

        });
        panels[Activepanel].webview.postMessage({ command: "increase", type: 'rx', value: selection.rx });
        panels[Activepanel].webview.postMessage({ command: "increase", type: 'ry', value: selection.ry });
    });
    vscode.commands.registerCommand('shapes.decreasebr', node => {
        if (selection.rx - 2 >= 0) {
            selection.rx = selection.rx - 2;
            selection.ry = selection.ry - 2;

            ribbon.forEach((ribbon_item, i) => {
                ribbon_item.hide();
                if (i === 6) {
                    ribbon_item.text = 'border-radius ' + selection.rx;
                }
                ribbon_item.show();

            });
            panels[Activepanel].webview.postMessage({ command: "decrease", type: 'rx', value: selection.rx });
            panels[Activepanel].webview.postMessage({ command: "decrease", type: 'ry', value: selection.ry });
        }

    });
    vscode.commands.registerCommand('shapes.increaseWidth', node => {
        selection.width = selection.width + 5;
        ribbon.forEach((ribbon_item, i) => {
            ribbon_item.hide();
            if (i === 0) {
                ribbon_item.text = 'width ' + selection.width;
            }
            ribbon_item.show();

        });
        panels[Activepanel].webview.postMessage({ command: "increase", type: 'width', value: selection.width });
    });
    vscode.commands.registerCommand('shapes.increaseHeight', node => {
        selection.height = selection.height + 5;
        ribbon.forEach((ribbon_item, i) => {
            ribbon_item.hide();
            if (i === 3) {
                ribbon_item.text = 'height ' + selection.height;
            }
            ribbon_item.show();

        });
        panels[Activepanel].webview.postMessage({ command: "increase", type: 'height', value: selection.height });
    });
    vscode.commands.registerCommand('shapes.decreaseWidth', node => {
        if (selection.width - 5 > 10) {
            ribbon.forEach((ribbon_item, i) => {
                ribbon_item.hide();
                if (i === 0) {
                    ribbon_item.text = 'width ' + selection.width;
                }
                ribbon_item.show();

            });
            selection.width = selection.width - 5;
            panels[Activepanel].webview.postMessage({ command: "decrease", type: 'width', value: selection.width });
        }
    });
    vscode.commands.registerCommand('shapes.decreaseHeight', node => {
        if (selection.height - 5 > 10) {
            ribbon.forEach((ribbon_item, i) => {
                ribbon_item.hide();
                if (i === 3) {
                    ribbon_item.text = 'height ' + selection.height;
                }
                ribbon_item.show();

            });
            selection.height = selection.height - 5;
            panels[Activepanel].webview.postMessage({ command: "decrease", type: 'height', value: selection.height });
        }
    });
    vscode.commands.registerCommand('shapes.deleteEntry', node => vscode.window.showInformationMessage('Successfully called delete entry'));


    vscode.commands.registerCommand('shapes.save', () => {
        panels.forEach((temp_panel, i) => {
            if (temp_panel.active) {
                try {
                    temp_panel.webview.postMessage({ command: "save_jpeg" });
                } catch (e) {
                    console.log(e);
                }
            }
        });
    });
}
function getSelection(message: any) {
    return AllShapes[message.panel_id].filter(x => { if (x.id === message.data.id) { return x; } })[0];
}
export function deactivate() {
}